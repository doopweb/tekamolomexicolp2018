"use strict";

WebFont.load({
    google: {
        families: [
            'Poppins:300,700',
            'Roboto:100,100italic,700,700italic',
            'Nunito+Sans:200,700,700italic,200italic',
            'Nunito+Sans:300,700,700italic,300italic',
            'Open+Sans:400,700,700italic,400italic',
        ]
    }
});